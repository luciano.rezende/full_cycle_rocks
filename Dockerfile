FROM golang:1.20-alpine AS builder

WORKDIR /app/
COPY go.mod main.go ./

RUN go build

FROM scratch

COPY --from=builder /app/full_cycle_rocks .

ENTRYPOINT ["./full_cycle_rocks"]
